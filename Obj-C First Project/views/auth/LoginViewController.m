//
//  LoginViewController.m
//  Obj-C First Project
//
//  Created by user on 03/04/19.
//  Copyright © 2019 sumedcoding. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) UIView *superview;

@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UITextField *tfUsername;
@property (nonatomic, strong) UITextField *tfPassword;
@property (nonatomic, strong) UIButton *btnLogin;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _superview = self.view;
    
    NSLog(@"VERSION : %@", [self getVersionString]);//DELETE SOON
    
    [self init_lblTitle];
    [self init_tfUsername];
    [self init_tfPassword];
    [self init_btnLogin];
    //TODO: Create btn register
    //TODO: Create label version text to footer, ex: v1.0.0.0, version: 1.0.0.0 or just 1.0.0.0
}

- (void) init_lblTitle{
    _lblTitle = [[UILabel alloc] init];
    [_lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_lblTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
    _lblTitle.text = @"Welcome To Obj-C First Project";
    [_superview addSubview:_lblTitle];
    
    [self addConstraint:_lblTitle :NSLayoutAttributeCenterX :NSLayoutRelationEqual :_superview :NSLayoutAttributeCenterX :1 :0];
    [self addConstraint:_lblTitle :NSLayoutAttributeTop :NSLayoutRelationEqual :_superview :NSLayoutAttributeTop :1 :30];
}

- (void) init_tfUsername{
    _tfUsername = [[UITextField alloc] init];
    [_tfUsername setBorderStyle:UITextBorderStyleRoundedRect];
    [_tfUsername setTranslatesAutoresizingMaskIntoConstraints:NO];
    _tfUsername.placeholder = @"Input your username";
    [_superview addSubview:_tfUsername];
    
    [self addConstraint:_tfUsername :NSLayoutAttributeCenterX :NSLayoutRelationEqual :_superview :NSLayoutAttributeCenterX :1 :0];
    [self addConstraint:_tfUsername :NSLayoutAttributeTop :NSLayoutRelationEqual :_lblTitle :NSLayoutAttributeBottom :1 :10];
    [self addConstraint:_tfUsername :NSLayoutAttributeWidth :NSLayoutRelationEqual :_superview :NSLayoutAttributeWidth :0.9 :0];
    [self addConstraint:_tfUsername :NSLayoutAttributeHeight :NSLayoutRelationEqual :nil :NSLayoutAttributeNotAnAttribute :1 :30];
}

- (void) init_tfPassword{
    _tfPassword = [[UITextField alloc] init];
    [_tfPassword setBorderStyle:UITextBorderStyleRoundedRect];
    [_tfPassword setTranslatesAutoresizingMaskIntoConstraints:NO];
    _tfPassword.placeholder = @"Input your password";
    _tfPassword.secureTextEntry = YES;
    [_superview addSubview:_tfPassword];
    
    [self addConstraint:_tfPassword :NSLayoutAttributeCenterX :NSLayoutRelationEqual :_superview :NSLayoutAttributeCenterX :1 :0];
    [self addConstraint:_tfPassword :NSLayoutAttributeTop :NSLayoutRelationEqual :_tfUsername :NSLayoutAttributeBottom :1 :10];
    [self addConstraint:_tfPassword :NSLayoutAttributeWidth :NSLayoutRelationEqual :_superview :NSLayoutAttributeWidth :0.9 :0];
    [self addConstraint:_tfPassword :NSLayoutAttributeHeight :NSLayoutRelationEqual :nil :NSLayoutAttributeNotAnAttribute :1 :30];
}

- (void) init_btnLogin{
    _btnLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_btnLogin addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    [_btnLogin setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_btnLogin setTitle:@"LOGIN" forState:UIControlStateNormal];
    [_superview addSubview:_btnLogin];
    
    [self addConstraint:_btnLogin :NSLayoutAttributeCenterX :NSLayoutRelationEqual :_superview :NSLayoutAttributeCenterX :1 :0];
    [self addConstraint:_btnLogin :NSLayoutAttributeTop :NSLayoutRelationEqual :_tfPassword :NSLayoutAttributeBottom :1 :10];
    [self addConstraint:_btnLogin :NSLayoutAttributeWidth :NSLayoutRelationEqual :_superview :NSLayoutAttributeWidth :0.9 :0];
    [self addConstraint:_btnLogin :NSLayoutAttributeHeight :NSLayoutRelationEqual :nil :NSLayoutAttributeNotAnAttribute :1 :30];
}

- (void) loginAction{
    //TODO: Move these codes to view model
    if([_tfUsername.text isEqualToString:@""] || [_tfPassword.text isEqualToString:@""]){
        [self showAlert:@"Login Failed" : @"Username and password must be field"];
    }else{
        [self showAlert:@"Login Successful" :[NSString stringWithFormat:@"Mr/Mrs %@", _tfUsername.text]];
    }
}

- (void) addConstraint:(UIView *)firstItem : (int)firstAttribute : (int)relatedBy : (UIView *)secondItem : (int)secondAttribute : (float)multiplier : (float) constant{
    NSLayoutConstraint *myConstraint;
    myConstraint = [NSLayoutConstraint
                    constraintWithItem:firstItem
                    attribute:firstAttribute
                    relatedBy:relatedBy
                    toItem:secondItem
                    attribute:secondAttribute
                    multiplier:multiplier
                    constant:constant];
    
    [_superview addConstraint:myConstraint];
}

- (void) showAlert:(NSString *) title :(NSString *) message{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //handler action
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSString *) getVersionString{
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    return version;
}

@end
